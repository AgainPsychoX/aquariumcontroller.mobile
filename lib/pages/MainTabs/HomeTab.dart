import 'package:flutter/material.dart';

import '../../util/ApplicationLocalizations.dart';

class HomeTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(ApplicationLocalizations.of(context).mainPage.menuHome)
    );
  }
}
