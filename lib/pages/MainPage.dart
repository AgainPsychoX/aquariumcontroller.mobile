import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tuple/tuple.dart';

import '../util/ApplicationLocalizations.dart';
import './MainTabs/HomeTab.dart';
import './MainTabs/StatusTab.dart';
import './MainTabs/ConnectionTab.dart';
import './MainTabs/AboutTab.dart';

enum MainPageTab {
  home,
  status,
  connection,
  about,
}

class MainPage extends StatefulWidget {
  final MainPageTab activeTab;

  MainPage({Key key, this.activeTab}) : super(key: key);

  @override
  _MainPageState createState() {
    return _MainPageState(this.activeTab);
  }

  static Tuple2<Widget, String> getTabData(BuildContext context, MainPageTab tabEnum) {
    final i10n = ApplicationLocalizations.of(context).mainPage;
    switch (tabEnum) {
      case MainPageTab.home:
        return Tuple2(HomeTab(), i10n.menuHome);
      case MainPageTab.status:
        return Tuple2(StatusTab(), i10n.menuStatus);
      case MainPageTab.connection:
        return Tuple2(ConnectionTab(), i10n.menuConnection);
      case MainPageTab.about:
        return Tuple2(AboutTab(), i10n.menuAbout);
      default:
        return Tuple2(Center(child: Text('Tab not found')), 'Tab not found');
    }
  }
}

class _MainPageState extends State<MainPage> {
  MainPageTab _activeTab;

  _MainPageState(this._activeTab);

  @override
  Widget build(BuildContext context) {
    final i10n = ApplicationLocalizations.of(context).mainPage;
    final data = MainPage.getTabData(context, this._activeTab);
    final Widget widget = data.item1;
    final String title = data.item2;
    
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            DrawerHeader(
              child: FittedBox(
                fit: BoxFit.contain,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    SvgPicture.asset('images/logo.svg'),
                    Text(ApplicationLocalizations.of(context).applicationTitle, style: (TextStyle style){
                      return style.merge(TextStyle(fontSize: style.fontSize + 10, color: Colors.white));
                    }(Theme.of(context).textTheme.title))
                  ],
                )
              ),
              decoration: const BoxDecoration(
                color: Colors.blue,
              ),
            ),
            ListTile(
              title: Text(i10n.menuHome, style: Theme.of(context).textTheme.subhead),
              leading: const Icon(Icons.home, color: Colors.black),
              onTap: () { setState(() { 
                _activeTab = MainPageTab.home; 
                Navigator.of(context).pop();
              }); }
            ),
            ListTile(
              title: Text(i10n.menuStatus, style: Theme.of(context).textTheme.subhead),
              leading: const Icon(Icons.check_circle_outline, color: Colors.black),
              onTap: () { setState(() { 
                _activeTab = MainPageTab.status;
                Navigator.of(context).pop();
              }); }
            ),
            ListTile(
              title: Text(i10n.menuLighting, style: Theme.of(context).textTheme.subhead),
              leading: const Icon(Icons.wb_incandescent, color: Colors.black),
            ),
            ListTile(
              title: Text(i10n.menuHeating, style: Theme.of(context).textTheme.subhead),
              leading: const Icon(Icons.whatshot /*ac_unit*/, color: Colors.black),
            ),
            ListTile(
              title: Text(i10n.menuTime, style: Theme.of(context).textTheme.subhead),
              leading: const Icon(Icons.access_time, color: Colors.black),
            ),
            ListTile(
              title: Text(i10n.menuConnection, style: Theme.of(context).textTheme.subhead),
              leading: const Icon(Icons.bluetooth, color: Colors.black),
              onTap: () { setState(() { 
                _activeTab = MainPageTab.connection;
                Navigator.of(context).pop();
              }); }
            ),
            ListTile(
              title: Text(i10n.menuAbout, style: Theme.of(context).textTheme.subhead),
              leading: const Icon(Icons.info, color: Colors.black),
              onTap: () { setState(() { 
                _activeTab = MainPageTab.about;
                Navigator.of(context).pop();
              }); }
            ),
          ]
        ),
      ),
      body: widget // @TODO . transition
    );
  }
}
