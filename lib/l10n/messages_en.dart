// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

// ignore: unnecessary_new
final messages = new MessageLookup();

// ignore: unused_element
final _keepAnalysisHappy = Intl.defaultLocale;

// ignore: non_constant_identifier_names
typedef MessageIfAbsent(String message_str, List args);

class MessageLookup extends MessageLookupByLibrary {
  get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "MainPageLocalizations_aboutCreatedBy" : MessageLookupByLibrary.simpleMessage("Created by"),
    "MainPageLocalizations_aboutSeeSourceApp" : MessageLookupByLibrary.simpleMessage("See Dart sources of this Flutter application"),
    "MainPageLocalizations_aboutSeeSourceDev" : MessageLookupByLibrary.simpleMessage("See C++ sources of the Arduino controller"),
    "MainPageLocalizations_aboutText1" : MessageLookupByLibrary.simpleMessage("I created this application for the aquarium controller while learning the technology of mobile applications. It uses Flutter framework - Google\'s portable UI toolkit for building beautiful, natively-compiled applications for mobile, web, and desktop from a single codebase."),
    "MainPageLocalizations_aboutText2" : MessageLookupByLibrary.simpleMessage("Whole Flutter runs by Dart language. Every element of the application, including UI, styling, translations and of course logic is written using Dart."),
    "MainPageLocalizations_aboutText3" : MessageLookupByLibrary.simpleMessage("The application connects to Arduino-based device, which runs real aquarium controller circuits. It has serval features, such as controlling lighting, heating, reporting temperatures, time and water pH level."),
    "MainPageLocalizations_aboutText4" : MessageLookupByLibrary.simpleMessage("The controller uses C++ to make things happen. The communication between it and the mobile application is realized over Bluetooth. There is also packet system to prevent data loss."),
    "MainPageLocalizations_aboutText5" : MessageLookupByLibrary.simpleMessage("The controller and the application was programmed by me for my dad who has an aquarium."),
    "MainPageLocalizations_menuAbout" : MessageLookupByLibrary.simpleMessage("About"),
    "MainPageLocalizations_menuConnection" : MessageLookupByLibrary.simpleMessage("Connection"),
    "MainPageLocalizations_menuHeating" : MessageLookupByLibrary.simpleMessage("Temperatures and heating"),
    "MainPageLocalizations_menuHome" : MessageLookupByLibrary.simpleMessage("Main page"),
    "MainPageLocalizations_menuLighting" : MessageLookupByLibrary.simpleMessage("Lighting"),
    "MainPageLocalizations_menuStatus" : MessageLookupByLibrary.simpleMessage("Overall status"),
    "MainPageLocalizations_menuTime" : MessageLookupByLibrary.simpleMessage("Controller time"),
    "applicationTitle" : MessageLookupByLibrary.simpleMessage("Aquarium controller")
  };
}
