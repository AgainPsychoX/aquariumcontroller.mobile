// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a pl locale. All the
// messages from the main program should be duplicated here with the same
// function name.

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

// ignore: unnecessary_new
final messages = new MessageLookup();

// ignore: unused_element
final _keepAnalysisHappy = Intl.defaultLocale;

// ignore: non_constant_identifier_names
typedef MessageIfAbsent(String message_str, List args);

class MessageLookup extends MessageLookupByLibrary {
  get localeName => 'pl';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "MainPageLocalizations_aboutCreatedBy" : MessageLookupByLibrary.simpleMessage("Stworzona przez"),
    "MainPageLocalizations_aboutSeeSourceApp" : MessageLookupByLibrary.simpleMessage("Sprawdź źródła Dart tej aplikacji Flutter"),
    "MainPageLocalizations_aboutSeeSourceDev" : MessageLookupByLibrary.simpleMessage("Sprawdź źródła C++ kontrolera Arduino"),
    "MainPageLocalizations_aboutText1" : MessageLookupByLibrary.simpleMessage("Aplikacja stworzona do kontrolera akwarium podczas nauki różnych technologii aplikacji mobilnych. Używa biblioteki Flutter - Wieloplatformowego narzędzia od Google do tworzenia pięknych, natywnie kompilowanych aplikacji mobilnych, webowych oraz desktopowych używając ujednoliconej bazy kodowej."),
    "MainPageLocalizations_aboutText2" : MessageLookupByLibrary.simpleMessage("Cały Flutter działa na języku Dart. Każdy element aplikacji, włączając w to elementy interfejsu, style, pliki tłumaczeń i oczywiście sama logika aplikacji są napisane w Dartcie."),
    "MainPageLocalizations_aboutText3" : MessageLookupByLibrary.simpleMessage("Aplikacja łączy się do urządzenia Android, które jest faktycznym kontrolerem akwarium. Obsługuje on kilka rzeczy, takich jak oświetlenie, ogrzewanie, podgląd temperatur, czasu czy też poziomu pH wody."),
    "MainPageLocalizations_aboutText4" : MessageLookupByLibrary.simpleMessage("Program kontrolera jest napisany w C++. Komunikacja między tym kontrolerem, a aplikacją mobilną jest realizowana przez Bluetooth. Dodatkowo jest użyty system pakietów, żeby zapobiec utracie danych."),
    "MainPageLocalizations_aboutText5" : MessageLookupByLibrary.simpleMessage("Kontroler i aplikacja zostały zaprogramowane przeze mnie dla mojego taty, który prowadzi akwarium."),
    "MainPageLocalizations_menuAbout" : MessageLookupByLibrary.simpleMessage("O aplikacji"),
    "MainPageLocalizations_menuConnection" : MessageLookupByLibrary.simpleMessage("Połączenie"),
    "MainPageLocalizations_menuHeating" : MessageLookupByLibrary.simpleMessage("Temperatury i ogrzewanie"),
    "MainPageLocalizations_menuHome" : MessageLookupByLibrary.simpleMessage("Strona główna"),
    "MainPageLocalizations_menuLighting" : MessageLookupByLibrary.simpleMessage("Oświetlenie"),
    "MainPageLocalizations_menuStatus" : MessageLookupByLibrary.simpleMessage("Status ogólny"),
    "MainPageLocalizations_menuTime" : MessageLookupByLibrary.simpleMessage("Czas sterownika"),
    "applicationTitle" : MessageLookupByLibrary.simpleMessage("Sterownik akwarium")
  };
}
