import 'dart:async';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../l10n/MainPageLocalizations.dart';
import '../l10n/messages_all.dart';

class ApplicationLocalizations {
  static Future<ApplicationLocalizations> load(Locale locale) {
    final String name = locale.countryCode == null ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);

    return initializeMessages(localeName).then((bool _) {
      Intl.defaultLocale = localeName;
      return ApplicationLocalizations();
    });
  }

  static ApplicationLocalizations of(BuildContext context) {
    return Localizations.of<ApplicationLocalizations>(context, ApplicationLocalizations);
  }
  
  String get applicationTitle { return Intl.message('Aquarium controller', name: 'applicationTitle', desc: 'The application title'); }
  
  MainPageLocalizations get mainPage { return MainPageLocalizations(); }
}

class ApplicationLocalizationsDelegate extends LocalizationsDelegate<ApplicationLocalizations> {
  const ApplicationLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) {
    return ['en', 'pl'].contains(locale.languageCode);
  }

  @override
  Future<ApplicationLocalizations> load(Locale locale) {
    return ApplicationLocalizations.load(locale);
  }

  @override
  bool shouldReload(LocalizationsDelegate<ApplicationLocalizations> old) {
    return false;
  }
}